module gitlab.com/flimzy/birka

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/otiai10/copy v1.0.2 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gitlab.com/flimzy/testy v0.0.1
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
)
