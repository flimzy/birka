package env

import (
	"os"
	"testing"

	"gitlab.com/flimzy/testy"

	"gitlab.com/flimzy/birka/cipher"
	"gitlab.com/flimzy/birka/cipher/base64"
	"gitlab.com/flimzy/birka/cipher/plaintext"
)

func TestEnviron(t *testing.T) {
	type tt struct {
		env map[string]string
	}
	tests := testy.NewTable()
	tests.Add("empty", tt{
		env: map[string]string{},
	})
	tests.Add("simple", tt{
		env: map[string]string{
			"FOO": "BAR",
			"BAR": "baz=foo",
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		defer testy.RestoreEnv()()
		os.Clearenv()
		_ = testy.SetEnv(tt.env)
		result := Environ()
		if d := testy.DiffInterface(testy.Snapshot(t), result); d != nil {
			t.Error(d)
		}
	})
}

func TestEncodeID(t *testing.T) {
	id := uint64(123)
	want := "ew"
	got := encodeID(id)
	if want != got {
		t.Errorf("Want: %s\n Got: %s\n", want, got)
	}
}

func TestDecodeID(t *testing.T) {
	t.Run("valid", func(t *testing.T) {
		in := "ew"
		want := uint64(123)
		got, err := decodeID(in)
		if err != nil {
			t.Fatal(err)
		}
		if want != got {
			t.Errorf("Want: %d\n Got: %d\n", want, got)
		}
	})
	t.Run("invalid varint", func(t *testing.T) {
		in := ""
		_, err := decodeID(in)
		testy.Error(t, "invalid varint", err)
	})
	t.Run("invalid base64", func(t *testing.T) {
		in := "&&&"
		_, err := decodeID(in)
		testy.Error(t, "illegal base64 data at input byte 0", err)
	})
}

var testCiphers = &Ciphers{
	ciphers: map[uint64]cipher.Cipher{
		0: plaintext.New(),
		1: base64.New(),
	},
}

func TestDecodeEnviron(t *testing.T) {
	type tt struct {
		ciphers   *Ciphers
		prefix    string
		env       map[string]string
		whitelist []string
		err       string
	}
	tests := testy.NewTable()
	tests.Add("nothing to decode, no whitelist", tt{
		ciphers: testCiphers,
		prefix:  "$$",
		env:     map[string]string{"FOO": "BAR"},
		err:     "FOO not encrypted and not in whitelist",
	})
	tests.Add("whitelisted", tt{
		ciphers:   testCiphers,
		prefix:    "$$",
		env:       map[string]string{"FOO": "BAR"},
		whitelist: []string{"FOO"},
	})
	tests.Add("base64 encoded", tt{
		ciphers: testCiphers,
		prefix:  "$$",
		env:     map[string]string{"FOO": "$$AQ.Zm9v"},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		result, err := tt.ciphers.DecodeEnviron(tt.env, tt.prefix, tt.whitelist)
		testy.Error(t, tt.err, err)
		if d := testy.DiffInterface(testy.Snapshot(t), result); d != nil {
			t.Error(d)
		}
	})
}
