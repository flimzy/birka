package env

import (
	"encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"os"
	"strings"

	"gitlab.com/flimzy/birka/cipher"
)

// Environ wraps os.Environ, and parses key/value pairs.
func Environ() map[string]string {
	env := os.Environ()
	result := make(map[string]string, len(env))
	for _, kv := range env {
		parts := strings.SplitN(kv, "=", 2)
		result[parts[0]] = parts[1]
	}
	return result
}

// Ciphers needs a beter name, and should be configured at runtime.
type Ciphers struct {
	ciphers map[uint64]cipher.Cipher
}

func (c *Ciphers) decode(id uint64, value string) (string, error) {
	dec, ok := c.ciphers[id]
	if !ok {
		return "", fmt.Errorf("invalid cipher id 0x%x", id)
	}
	return dec.Decode(value)
}

func encodeID(id uint64) string {
	buf := make([]byte, 9)
	c := binary.PutUvarint(buf, id)
	return base64.RawStdEncoding.EncodeToString(buf[:c])
}

func decodeID(id string) (uint64, error) {
	buf, err := base64.RawStdEncoding.DecodeString(id)
	if err != nil {
		return 0, err
	}
	val, n := binary.Uvarint(buf)
	if n <= 0 {
		return val, errors.New("invalid varint")
	}
	return val, nil
}

// DecodeEnviron is the core of the application. It iterates through env, and
// decodes any encoded values. Any non-encoded values not found in whitelist
// will result in an error being returned.
func (c *Ciphers) DecodeEnviron(env map[string]string, prefix string, whitelist []string) (map[string]string, error) {
	if prefix == "" {
		return nil, errors.New("prefix must be defined")
	}
	wl := make(map[string]struct{}, len(whitelist))
	for _, l := range whitelist {
		wl[l] = struct{}{}
	}
	for key, value := range env {
		if strings.HasPrefix(value, prefix) {
			parts := strings.SplitN(strings.TrimPrefix(value, prefix), ".", 2)
			if len(parts) < 2 {
				return nil, fmt.Errorf("%s unparseable: Invalid encoding", key)
			}
			id, err := decodeID(parts[0])
			if err != nil {
				return nil, fmt.Errorf("%s unparseable: %w", key, err)
			}
			result, err := c.decode(id, parts[1])
			if err != nil {
				return nil, err
			}
			env[key] = result
			continue
		}
		if _, ok := wl[key]; !ok {
			return nil, fmt.Errorf("%s not encrypted and not in whitelist", key)
		}
	}
	return env, nil
}
