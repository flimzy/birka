# Birka

Birka aims to facilitate secure handling of environment variables, particularly for CI environments, in a way that is not tied to any CI vendor.

⚠️ ⚠️ WARNING ⚠️ ⚠️

Birka is a work-in-progress, and an experiment. It is not yet usable, and once it is, it should be considered insecure until further notice.

⚠️ ⚠️ WARNING ⚠️ ⚠️

# About the name

"Birka" gets its name from:

> An ancient settlement on the island of Björkö in Lake Mälaren in Sweden. When the trading center of Helgö declined, Birka arose, just 10 km away. It is thought that merchant trade was moved to Birka in the 9th century because the king wanted to pursue trade outside the nation’s boundaries and the sea-faring ships of the day, cogs, needed a deeper harbor than Helgö could offer. Its position also gave Birka better conditions to grow to a city. Archeological studies since the late 19th century have turned up large numbers of keys, locks and parts of locks. See the article on the History of keys.

[source](https://www.historicallocks.com/en/site/h/historicallocks/dictionary/)

# License

This software is released under the terms of the MIT license. See the [LICENCE][LICENSE] file for details.
