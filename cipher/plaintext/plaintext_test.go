package plaintext

import (
	"testing"

	"gitlab.com/flimzy/testy"
)

func TestEncode(t *testing.T) {
	type tt struct {
		value string
		want  string
	}
	tests := testy.NewTable()
	tests.Add("normal", tt{
		value: "foo",
		want:  "foo",
	})
	tests.Add("escaped", tt{
		value: "foo, bar",
		want:  "foo,, bar",
	})

	c := &c{}
	tests.Run(t, func(t *testing.T, tt tt) {
		got, err := c.Encode(tt.value)
		if err != nil {
			t.Fatal(err)
		}
		if got != tt.want {
			t.Errorf("Want: %s\n Got: %s\n", tt.want, got)
		}
	})
}

func TestDecode(t *testing.T) {
	type tt struct {
		value string
		want  string
	}
	tests := testy.NewTable()
	tests.Add("normal", tt{
		value: "foo",
		want:  "foo",
	})
	tests.Add("escaped", tt{
		value: "foo,, bar",
		want:  "foo, bar",
	})

	c := &c{}
	tests.Run(t, func(t *testing.T, tt tt) {
		got, err := c.Decode(tt.value)
		if err != nil {
			t.Fatal(err)
		}
		if got != tt.want {
			t.Errorf("Want: %s\n Got: %s\n", tt.want, got)
		}
	})
}
