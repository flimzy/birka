// Package plaintext provides a plaintext "cipher". It only modifies the value
// in that it doubles commas, as an escaping mechanism.
package plaintext

import (
	"strings"

	"gitlab.com/flimzy/birka/cipher"
)

type c struct{}

var _ cipher.Cipher = &c{}

// New returns a new plaintext cipher instance.
func New() cipher.Cipher {
	return &c{}
}

func (c) Encode(value string) (string, error) {
	return strings.ReplaceAll(value, ",", ",,"), nil
}

func (c) Decode(value string) (string, error) {
	return strings.ReplaceAll(value, ",,", ","), nil
}
