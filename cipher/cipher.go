package cipher

type err string

func (e err) Error() string {
	return string(e)
}

const (
	// ErrNotSupported is returned by a cipher if it receives an unsupported
	// cipher format.
	ErrNotSupported = err("input value not supported by cipher")
	// ErrUnrecognizedCipher is returned when none of the registered ciphers
	// can decode a value.
	ErrUnrecognizedCipher = err("unable to decode value; unrecognized cipher")
)

// Cipher is a generic interface to a supported cipher.
type Cipher interface {
	Encode(value string) (string, error)
	// Decode is passed a raw (possibly encrypted) env variable value, and is
	// expected to decode it. If the cipher cannot decode it, it should return
	// ErrNotSupported
	Decode(value string) (string, error)
}
