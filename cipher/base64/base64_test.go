package base64

import (
	"testing"

	"gitlab.com/flimzy/testy"
)

func TestEncode(t *testing.T) {
	type tt struct {
		value string
		want  string
	}
	tests := testy.NewTable()
	tests.Add("normal", tt{
		value: "foo",
		want:  "Zm9v",
	})
	tests.Add("escaped", tt{
		value: "foo, bar",
		want:  "Zm9vLCBiYXI",
	})

	c := &c{}
	tests.Run(t, func(t *testing.T, tt tt) {
		got, err := c.Encode(tt.value)
		if err != nil {
			t.Fatal(err)
		}
		if got != tt.want {
			t.Errorf("Want: %s\n Got: %s\n", tt.want, got)
		}
	})
}

func TestDecode(t *testing.T) {
	type tt struct {
		value string
		want  string
		err   string
	}
	tests := testy.NewTable()
	tests.Add("normal", tt{
		value: "Zm9v",
		want:  "foo",
	})
	tests.Add("escaped", tt{
		value: "Zm9vLCBiYXI",
		want:  "foo, bar",
	})
	tests.Add("invalid encoding", tt{
		value: "xxxxx",
		err:   "illegal base64 data at input byte 4",
	})

	c := &c{}
	tests.Run(t, func(t *testing.T, tt tt) {
		got, err := c.Decode(tt.value)
		testy.Error(t, tt.err, err)
		if got != tt.want {
			t.Errorf("Want: %s\n Got: %s\n", tt.want, got)
		}
	})
}
