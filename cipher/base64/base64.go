// Package base64 provides a base64 "cipher". This exists primarily for testing
// and debugging purposes, but may also be useful when it's useful to set an
// environment variable that contains non-printable characters.
package base64

import (
	"encoding/base64"

	"gitlab.com/flimzy/birka/cipher"
)

type c struct{}

var _ cipher.Cipher = &c{}

// New returns a new base64 cipher instance.
func New() cipher.Cipher {
	return &c{}
}

func (c) Encode(value string) (string, error) {
	return base64.RawStdEncoding.EncodeToString([]byte(value)), nil
}

func (c) Decode(value string) (string, error) {
	result, err := base64.RawStdEncoding.DecodeString(value)
	return string(result), err
}
